package com.aniket.websocketexperiment.controller;

import com.aniket.websocketexperiment.request.SSERequest;
import com.aniket.websocketexperiment.response.SSEResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@Slf4j
public class ServerSideEventController {

    private List<SseEmitter> sseEmitterList = new ArrayList<>();

    @GetMapping("/listen-to-sse")
    public SseEmitter emitMessages(){
        SseEmitter sseEmitter = new SseEmitter(9999999999999l);
        //HTML5 introduce Event Source Api which can Use this event
        //Save reference to send events
        sseEmitterList.add(sseEmitter);
        //Client will Keep on listening here
        sseEmitter.onCompletion(()->{
            log.info("Client is Now Dropped/Finished -- {}",sseEmitter);
            sseEmitterList.remove(sseEmitter);
        });
        return sseEmitter;

    }

    @PostMapping("/publish-asset-update")
    public SSEResponse publishAsset(@RequestBody SSERequest sseRequest){
        log.info("Published Asset Data -- {}",sseRequest);
        //First Put somewhere in Persistence layer then Emit it
        sseEmitterList.stream().forEach(item->{
            try {
                SseEmitter.SseEventBuilder sseEventBuilder =  SseEmitter.event().name(sseRequest.getEventType().name())
                        .data(sseRequest.getPayload()+"_"+sseRequest.getEventType().name()+"_"+new Date(), MediaType.TEXT_PLAIN)
                        .comment("This is a personal Comment !!! ")
                        .id(UUID.randomUUID().toString())
                        .reconnectTime(1000);
                log.info("Event -- {}",sseEventBuilder);
                item.send( sseEventBuilder);
                log.info("Event Published !!! - {}",sseRequest);
            } catch (IOException e) {
              log.info("--Error Happened for Emitter - {} ",item);
            }
        });
        return new SSEResponse(true,new Date());
    }
}
