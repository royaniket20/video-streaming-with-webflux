package com.aniket.websocketexperiment.response;

import com.aniket.websocketexperiment.constants.SSEEventEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class SSEResponse {
   private Boolean status;
   private Date publishTime;
}
