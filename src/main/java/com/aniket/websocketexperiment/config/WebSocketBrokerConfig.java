package com.aniket.websocketexperiment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketBrokerConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
      //These Endpoints will be SockJs endpoint will be used by STOMP

        //Websocket have only the First Call HTTP where handshaking happens as well coneection Upgrade happens
        //This is the Only chance where you can Do header Reading and auth mechanism - After connection update No more Http

        registry.addEndpoint("/echo-data-sockjs")
                .setHandshakeHandler(new WebsocketHandshakeHandler())
                .withSockJS();
    }

    //Defining Message Broker which will send Message from Stomp Client to correct Destination []
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app") //All this will direct to Our Application Message Handler for a Topic Name
                //.enableSimpleBroker("/topic" , "/queue") ;;// Embedded Broker - Not Good for Scaling
                .enableStompBrokerRelay("/topic" , "/queue")
                .setUserDestinationBroadcast("/topic/unresolved-user-dest") //When Two Users are On Different Server - this property will relay all
                //Unresolved message to Global level ya Rabbit MQ

                // Generally a Spring application use a Default strategy [DefaultSimpleUserRegistry] - to keep tract to active connected users in apps
                //But on a Microservice where Application running 304 Instances --- Spring need to use [MultiServerUserRegistry] -- Or Use Redis
                //Else servers will only get only its Local View
                .setUserRegistryBroadcast("/topic/user-global-registry")
                .setRelayHost("sparrow.rmq.cloudamqp.com").setRelayPort(61613 )
                .setVirtualHost("ickifmim")
                .setSystemLogin("ickifmim")
                .setSystemPasscode("FMo7s0ErA9Bna_rFKVkyk4WA7Rsn1mhr")
                .setClientLogin("ickifmim")
                .setClientPasscode("FMo7s0ErA9Bna_rFKVkyk4WA7Rsn1mhr");
                //.setVirtualHost("sparrow.rmq.cloudamqp.com");
    }




}
