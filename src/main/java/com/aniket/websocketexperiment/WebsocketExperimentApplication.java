package com.aniket.websocketexperiment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketExperimentApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketExperimentApplication.class, args);
	}

}
