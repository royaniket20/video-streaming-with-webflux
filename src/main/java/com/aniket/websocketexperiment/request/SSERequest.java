package com.aniket.websocketexperiment.request;

import com.aniket.websocketexperiment.constants.SSEEventEnum;
import lombok.Data;

@Data
public class SSERequest {
    private SSEEventEnum eventType;
    private String payload;
}
